//
//  main.m
//  component_A
//
//  Created by cdczx on 2017/10/26.
//  Copyright © 2017年 cdczx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
